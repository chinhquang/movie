//
//  FeatureViewController.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/28/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
class FeatureViewController: UIViewController{
    var listTopRatedMovie : [MovieDetail] = [MovieDetail](){
        didSet{
            topRatedCollectionView.reloadData()
        }
    }
    var listPopularMovie : [MovieDetail] = [MovieDetail](){
        didSet{
            popularMovieCollectionView.reloadData()
        }
    }
    var listNowPlayingMovie : [MovieDetail] = [MovieDetail](){
        didSet{
            nowPlayingMovieCollectionView.reloadData()
        }
    }
    var listPopularPeople : [PersonDetail] = [PersonDetail](){
        didSet{
            popularPeopleCollectionView.reloadData()
        }
    }
    static var chosenMovie : MovieDetail = MovieDetail()
    static var chosenPerson : PersonDetail = PersonDetail()
    
    @IBAction func seeAll(_ sender: UIButton) {
        if sender.tag == 2{
            let nextViewController = PeopleListViewController()
            
            navigationController?.pushViewController(nextViewController,animated: true)
        }else {
            MovieListViewController.id = sender.tag
            let nextViewController = MovieListViewController()
            
            navigationController?.pushViewController(nextViewController,animated: true)
        }
        
        
    }
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var popularMovieCollectionView: UICollectionView!
    @IBOutlet weak var topRatedCollectionView: UICollectionView!
    @IBOutlet weak var nowPlayingMovieCollectionView: UICollectionView!
    @IBOutlet weak var popularPeopleCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        self.resetNavigationBar()
        setupView()
        setTopRatedMovieList()
        setPopularMovieList()
        setNowPlayingMovieList()
        setPopularPeopleList()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.listPopularMovie.removeAll()
        self.listPopularPeople.removeAll()
        self.listTopRatedMovie.removeAll()
        self.listNowPlayingMovie.removeAll()
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        popularMovieCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        nowPlayingMovieCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func setupView(){
        self.navigationItem.title = "Discover"
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        
        topRatedCollectionView.register(UINib(nibName: "backDropCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "backDropCollectionViewCell")
        
        popularMovieCollectionView.register(UINib(nibName: "PosterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PosterCollectionViewCell")
        nowPlayingMovieCollectionView.register(UINib(nibName: "PosterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PosterCollectionViewCell")
        
        popularPeopleCollectionView.register(UINib(nibName: "PopularPeopleViewCell", bundle: nil), forCellWithReuseIdentifier: "PopularPeopleViewCell")
        
        
        self.topRatedCollectionView.delegate = self
        self.topRatedCollectionView.dataSource = self
        
        self.popularMovieCollectionView.delegate = self
        self.popularMovieCollectionView.dataSource = self
        
        self.nowPlayingMovieCollectionView.delegate = self
        self.nowPlayingMovieCollectionView.dataSource = self
        
        self.popularPeopleCollectionView.delegate = self
        self.popularPeopleCollectionView.dataSource = self
        
    }
    override func viewWillLayoutSubviews() {
        super .viewWillLayoutSubviews()
        self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: 773)
    }
    func setTopRatedMovieList() {
        API.getJSONResponse(baseURL: API.baseURLTopRatedMovies, params: API.getParamMovieList(page: 1)){ result in
            
            let results =  result["results"] as! NSArray
            //print(x)
            self.listTopRatedMovie.removeAll()
            for i in results{
                
                let t = i as! [String:Any]
                if let item = MovieDetail(JSON: t){
                    self.listTopRatedMovie.append(item)
                }
                
                
                
            }
            
            
        }
    }
    func setPopularMovieList() {
        API.getJSONResponse(baseURL: API.baseURLPopularMovies, params: API.getParamMovieList(page: 1)){ result in
            
            let results =  result["results"] as! NSArray
            //print(x)
            self.listPopularMovie.removeAll()
            for i in results{
                
                let t = i as! [String:Any]
                
                if let item = MovieDetail(JSON: t){
                    self.listPopularMovie.append(item)
                }
            }
            
        }
    }

    func setNowPlayingMovieList() {
        API.getJSONResponse(baseURL: API.baseURLNowPlayingMovies, params: API.getParamMovieList(page: 1)){ result in
            
            let results =  result["results"] as! NSArray
            //print(x)
            self.listNowPlayingMovie.removeAll()
            for i in results{
                
                let t = i as! [String:Any]
                if let item = MovieDetail(JSON: t){
                    self.listNowPlayingMovie.append(item)
                }
                
                
                
            }
            
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func setPopularPeopleList(){
        API.getJSONResponse(baseURL: API.baseURLPopularPeople, params: API.getParamMovieList(page: 1)){ result in
            
            let results =  result["results"] as! NSArray
            //print(x)
            self.listPopularPeople.removeAll()
            for i in results{
                
                let t = i as! [String:Any]
                if let item = PersonDetail(JSON: t){
                    self.listPopularPeople.append(item)
                }
                
                
                
            }
            
            
        }
    }
}



extension FeatureViewController :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == topRatedCollectionView{
            return listTopRatedMovie.count
        }
        else if collectionView == popularMovieCollectionView{
            return listPopularMovie.count
        }
        else if collectionView == nowPlayingMovieCollectionView{
            return listNowPlayingMovie.count
        }else if collectionView == popularPeopleCollectionView{
            return listPopularPeople.count
        }
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = UICollectionViewCell()
        if collectionView == topRatedCollectionView{
            let cell2 = topRatedCollectionView.dequeueReusableCell(withReuseIdentifier: "backDropCollectionViewCell", for: indexPath) as! backDropCollectionViewCell
            
            cell2.backgroundColor = UIColor.black
            
            if let image = listTopRatedMovie[indexPath.item].backdrop_path
            {
                if let tittle = listTopRatedMovie[indexPath.item].tittle
                {
                    
                    cell2.setCell(image: image, tittle: tittle)
                }
            }
            
            return cell2
        }
        else if collectionView == popularMovieCollectionView{
            let cell2 = popularMovieCollectionView.dequeueReusableCell(withReuseIdentifier: "PosterCollectionViewCell", for: indexPath) as! PosterCollectionViewCell
            
            cell2.backgroundColor = UIColor.black
            
            cell2.setCell(detail: listPopularMovie[indexPath.item])
            
            return cell2
        }
        else if collectionView == nowPlayingMovieCollectionView{
            let cell2 = nowPlayingMovieCollectionView.dequeueReusableCell(withReuseIdentifier: "PosterCollectionViewCell", for: indexPath) as! PosterCollectionViewCell
            
            cell2.backgroundColor = UIColor.black
            
            cell2.setCell(detail: listNowPlayingMovie[indexPath.item])
            
            return cell2
        }
        else if collectionView == popularPeopleCollectionView{
            let cell2 =  popularPeopleCollectionView.dequeueReusableCell(withReuseIdentifier: "PopularPeopleViewCell", for: indexPath) as! PopularPeopleViewCell
            cell2.setCell(detail: listPopularPeople[indexPath.item])
            return cell2
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == topRatedCollectionView{
            let nextViewController = MovieDetailViewController()
            FeatureViewController.chosenMovie = listTopRatedMovie[indexPath.item]
            navigationController?.pushViewController(nextViewController,animated: true)
        }
        else if collectionView == popularMovieCollectionView{
            let nextViewController = MovieDetailViewController()
            FeatureViewController.chosenMovie = listPopularMovie[indexPath.item]
            navigationController?.pushViewController(nextViewController,animated: true)
        }
        else if collectionView == nowPlayingMovieCollectionView{
            let nextViewController = MovieDetailViewController()
            FeatureViewController.chosenMovie = listNowPlayingMovie[indexPath.item]
            navigationController?.pushViewController(nextViewController,animated: true)
        }
        else if collectionView == popularPeopleCollectionView{
            let nextViewController = PersonDetailViewController()
            FeatureViewController.chosenPerson = listPopularPeople[indexPath.item]
            print(listPopularPeople[indexPath.item].id as Any)
        navigationController?.pushViewController(nextViewController,animated: true)
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == popularMovieCollectionView || collectionView == nowPlayingMovieCollectionView{
            return CGSize(width: 65, height: 125)
        }else if collectionView == topRatedCollectionView{
            return CGSize(width: 250, height: 125)
        }
        return CGSize(width: 85, height: 125)
    }
    
}
