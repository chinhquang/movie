//
//  TrailerListViewController.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/18/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class TrailerListViewController: UIViewController {
    
    var listTrailer : [String] = [String](){
        didSet{
            table.reloadData()
        }
    }
    func setTrailerKeyList() {
        
        if let id = FeatureViewController.chosenMovie.id{
            API.getJSONResponse(baseURL: API.getTrailerKeyURL(id: "\(id)"), params: API.getDefaultParam()){
                result in
                let results =  result["results"] as! NSArray
                //print(x)
                self.listTrailer.removeAll()
                for i in results{
                    
                    let t = i as! [String:Any]
                    if let item = TrailerDetail(JSON: t){
                        if let key = item.key{
                            self.listTrailer.append(key)
                        }
                        
                    }
                }
                
            }
        }
        
    }
    

    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        table.register(UINib(nibName: "TrailerTableViewCell", bundle: nil), forCellReuseIdentifier: "TrailerTableViewCell")
        table.delegate = self
        table.dataSource = self
        table.tableFooterView = UIView()
        setTrailerKeyList()
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TrailerListViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listTrailer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell1 = table.dequeueReusableCell(withIdentifier: "TrailerTableViewCell", for: indexPath) as! TrailerTableViewCell
        cell1.getVideo(videoCode: listTrailer[indexPath.item])
        return cell1
        
        
    }
  
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.width * 2 / 3
    }
    
}
