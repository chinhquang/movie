//
//  TabbarViewController.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/28/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
class TabbarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.barTintColor = UIColor.black
        //tab feature
        
        let featureVC = FeatureViewController()
        
        featureVC.tabBarItem = UITabBarItem(title: "Feature", image: #imageLiteral(resourceName: "feature"), tag: 0)
        
        //tab Search
        let searchVC = SearchViewController()
        
        searchVC.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 1)
        
        let topRatedVC = TopRatedViewController()
        topRatedVC.tabBarItem = UITabBarItem(tabBarSystemItem: .topRated, tag: 2)
        
        let cinemaVC = CinemaViewController()
        cinemaVC.tabBarItem = UITabBarItem(title: "Cinema", image: #imageLiteral(resourceName: "cinema"), tag: 3)
        let nav1 = UINavigationController (rootViewController: featureVC)
        let nav2 = UINavigationController (rootViewController: searchVC)
        let nav3 = UINavigationController (rootViewController: topRatedVC)
        let nav4 = UINavigationController (rootViewController: cinemaVC)
        let tabBarList = [nav1, nav2,nav3,nav4]
        
        viewControllers = tabBarList
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
