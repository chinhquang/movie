//
//  PeopleListViewController.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/7/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class PeopleListViewController: UIViewController {
    var list : [PersonDetail] = [PersonDetail](){
        didSet{
            table.reloadData()
        }
    }
    var canGoBack = false
    var listMovieFeature : [MovieDetail] = [MovieDetail]()
    var pageIndex : Int = 0
    var refreshControl : UIRefreshControl = UIRefreshControl()
    @IBOutlet weak var table: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        canGoBack = false
        self.table.tableFooterView = UIView()
        setupView()
        AddPullToRefresh()
    }
    override func viewWillDisappear(_ animated: Bool) {
        if canGoBack == false {
            super.viewWillDisappear(true)
            self.list.removeAll()
            
            self.removeFromParent()
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    func AddPullToRefresh (){
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attributes)
        
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        table.addSubview(refreshControl) // not required when using UITableViewController
    }
    @objc func refresh() {
        pageIndex = 0
        list.removeAll()
        loadMoreData()
        refreshControl.endRefreshing();
    }
    func setupView() {
        self.navigationItem.title = "List People"
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        
        table.register(UINib(nibName: "PersonDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "PersonDetailTableViewCell")
        table.delegate = self
        table.dataSource = self
        loadMoreData()
    }
    func loadMoreData() {
        pageIndex = pageIndex + 1
        setPopularPeopleList(pageIndex: pageIndex)
    }
    func setPopularPeopleList(pageIndex : Int) {
        API.getJSONResponse(baseURL: API.baseURLPopularPeople, params: API.getParamMovieList(page: pageIndex)){ result in
            
            let results =  result["results"] as! NSArray
            
            //            self.list.removeAll()
            for i in results{
                
                if let detail = i as? [String:Any]{
                    if let x = detail["known_for"] as? NSArray{
                        if let x0 = x[0] as? [String:Any]{
                            if let movieDetailItem = MovieDetail(JSON: x0)
                            {
                                self.listMovieFeature.append(movieDetailItem)
                            }
                        }
                        
                    }
                    
                }
                let t = i as! [String:Any]
                if let item = PersonDetail(JSON: t){
                    self.list.append(item)
                }
                
            }
            
            
        }
    }


}
extension PeopleListViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: "PersonDetailTableViewCell", for: indexPath) as! PersonDetailTableViewCell
        cell.selectionStyle = .none
        cell.setCell(detail: list[indexPath.row], movieDetail: listMovieFeature[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextViewController = PersonDetailViewController()
        FeatureViewController.chosenPerson = list[indexPath.row]
        
        canGoBack = true
        
    navigationController?.pushViewController(nextViewController,animated: true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = list.count - 1
        if indexPath.item == lastItem
        {
            loadMoreData()
        }
    }
    
    
}
