//
//  MovieDetailViewController.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/28/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
import SafariServices
import CoreData
class MovieDetailViewController: UIViewController{
    @IBOutlet weak var ratingController: RatingController!
    @IBOutlet weak var RateBtn: UIButton!

    @IBOutlet weak var vote_average: UILabel!
    @IBOutlet weak var genres: UILabel!
    @IBOutlet weak var homepage: UILabel!
    @IBOutlet weak var release_date: UILabel!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var subviewLoading: UIView!
    @IBOutlet weak var separator: UIView!
    var canGoBack : Bool = false
    @IBOutlet weak var parentView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBAction func segmentClick(_ sender: Any) {
        if segment.selectedSegmentIndex == 0{
            addOverviewView()
        }else if segment.selectedSegmentIndex == 1 {
            addPeopleSubview()
        }else if segment.selectedSegmentIndex == 2 {
            addSimilarSubview()
        }

    }
    @IBAction func rate(_ sender: Any) {
        
        if is_inRateList == false{
            saveRecord(entityname: "RateList")
            is_inRateList = true
            RateBtn.setTitleColor(UIColor.white, for: .normal)
        }else {
            is_inRateList = false
            RateBtn.setTitleColor(UIColor.orange, for: .normal)
            if let id = FeatureViewController.chosenMovie.id{
                deleteData(id: id, entityname: "RateList")
            }
        }
    }
    let watchListBtn = UIButton(type: .system)
    let bookmarkBtn = UIButton(type: .system)
    var is_inWatchList = false
    var is_inRateList = false
    var is_inFavoriteList = false
    var overview = OverviewSubview()
    var people = PeopleSubview()
    var similar  = SimilarSubview()
    var listTrailer : [String] = [String](){
        didSet{
            overview.trailerCollectionView.reloadData()
        }
    }
    var listCast : [CastDetail] = [CastDetail](){
        didSet{
            people.castCollectionview.reloadData()
        }
    }
    var listCrew : [CrewDetail] = [CrewDetail](){
        didSet{
            people.crewCollectionview.reloadData()
        }
    }
    var listSimilarMovies1 : [MovieDetail] = [MovieDetail](){
        didSet{
            similar.firstLineCollectionview.reloadData()
        }
    }
    var listSimilarMovies2 : [MovieDetail] = [MovieDetail](){
        didSet{
            similar.secondLineCollectionview.reloadData()
        }
    }
    func deleteData(id: Int, entityname : String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityname)
        fetchRequest.predicate = NSPredicate(format: "id = %d", id)
        
       
        
        do {
            let results = try context.fetch(fetchRequest)
            let objectDelete = results[0] as NSManagedObject
            context.delete(objectDelete)
            do{
                try context.save()
            }catch{
                print(error)
            }
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        
    }
    func someEntityExists(id: Int, entityname : String) -> Bool {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityname)
        fetchRequest.predicate = NSPredicate(format: "id = %d", id)
        
        var results: [NSManagedObject] = []
        
        do {
            results = try context.fetch(fetchRequest)
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        return results.count > 0
    }
    
    
    func addPeopleSubview(){
        for subUIView in subviewLoading.subviews as [UIView] {
            subUIView.removeFromSuperview()
        }
        
        
        
        people = PeopleSubview()
        people.awakeFromNib()
        subviewLoading.addSubview(people)
        
        //delegate ...
        people.castCollectionview.delegate = self
        people.crewCollectionview.delegate = self
        people.castCollectionview.dataSource = self
        people.crewCollectionview.dataSource = self
        
        people.castCollectionview.register(UINib(nibName: "PosterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PosterCollectionViewCell")
        people.crewCollectionview.register(UINib(nibName: "PosterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PosterCollectionViewCell")
        
        
        subviewLoading.topAnchor.constraint(equalTo: separator.bottomAnchor, constant: 16.5).isActive = true
        subviewLoading.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant: 49 ).isActive = true
        subviewLoading.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: 8).isActive = true
        subviewLoading.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: 8).isActive = true
        people.frame =  subviewLoading.bounds
        people.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setPeopleInfo()
    }
    func addSimilarSubview(){
        for subUIView in subviewLoading.subviews as [UIView] {
            subUIView.removeFromSuperview()
        }
        
        
        
        similar = SimilarSubview()
        similar.awakeFromNib()
        subviewLoading.addSubview(similar)
        
        //delegate ...
        similar.firstLineCollectionview.delegate = self
        similar.secondLineCollectionview.delegate = self
        similar.firstLineCollectionview.dataSource = self
        similar.secondLineCollectionview.dataSource = self
        
        similar.firstLineCollectionview.register(UINib(nibName: "PosterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PosterCollectionViewCell")
        similar.secondLineCollectionview.register(UINib(nibName: "PosterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PosterCollectionViewCell")
        
        
        subviewLoading.topAnchor.constraint(equalTo: separator.bottomAnchor, constant: 16.5).isActive = true
        subviewLoading.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant: 49 ).isActive = true
        subviewLoading.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: 8).isActive = true
        subviewLoading.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: 8).isActive = true
        similar.frame =  subviewLoading.bounds
        similar.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setSimilarInfo()
    }
    func addOverviewView() {
        for subUIView in subviewLoading.subviews as [UIView] {
            subUIView.removeFromSuperview()
        }
        
        
        
        overview = OverviewSubview()
        overview.awakeFromNib()
        subviewLoading.addSubview(overview)
        
        
        overview.trailerCollectionView.delegate = self
        overview.trailerCollectionView.dataSource = self
        
        overview.trailerCollectionView.register(UINib(nibName: "TrailerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TrailerCollectionViewCell")
        
        
        subviewLoading.topAnchor.constraint(equalTo: separator.bottomAnchor, constant: 16.5).isActive = true
        subviewLoading.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant: 49 ).isActive = true
        subviewLoading.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: 8).isActive = true
        subviewLoading.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: 8).isActive = true
        overview.frame =  subviewLoading.bounds
        overview.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setTrailerKeyList()
        setOverviewInfo()
    }
    func setupLeftBarItem(){
        
        
        
        
        watchListBtn.setImage(#imageLiteral(resourceName: "lookup").withRenderingMode(.automatic), for: .normal)
        watchListBtn.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        watchListBtn.addTarget(self, action: #selector(watchListClick), for: .touchUpInside)
        
        
        bookmarkBtn.setImage(#imageLiteral(resourceName: "star").withRenderingMode(.automatic), for: .normal)
        bookmarkBtn.addTarget(self, action: #selector(favoriteClick), for: .touchUpInside)
        bookmarkBtn.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        
        
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: watchListBtn),UIBarButtonItem(customView: bookmarkBtn)]
        if let id = FeatureViewController.chosenMovie.id{
            if someEntityExists(id: id, entityname: "WatchList") == true{
                is_inWatchList = true
                watchListBtn.setImage(#imageLiteral(resourceName: "icons8-opera-glasses-filled-50"), for: .normal)
            }
            if someEntityExists(id: id, entityname: "FavoriteList") == true{
                is_inFavoriteList = true
                bookmarkBtn.setImage(#imageLiteral(resourceName: "starfill"), for: .normal)
            }
            if someEntityExists(id: id, entityname: "RateList") == true{
                is_inRateList = true
                RateBtn.setTitleColor(UIColor.white, for: .normal)
            }
           
        }
        
    }
    @objc func watchListClick(){
        if is_inWatchList == false{
            is_inWatchList = true
            watchListBtn.setImage(#imageLiteral(resourceName: "icons8-opera-glasses-filled-50"), for: .normal)
            saveRecord(entityname: "WatchList")
        }else {
            is_inWatchList = false
            watchListBtn.setImage(#imageLiteral(resourceName: "lookup"), for: .normal)
            if let id = FeatureViewController.chosenMovie.id{
                deleteData(id: id, entityname: "WatchList")
            }
            
            
        }
        
    }
    @objc func favoriteClick(){
        if is_inFavoriteList == false{
            is_inFavoriteList = true
            bookmarkBtn.setImage(#imageLiteral(resourceName: "starfill"), for: .normal)
            saveRecord(entityname: "FavoriteList")
        }else{
            is_inFavoriteList = false
            bookmarkBtn.setImage(#imageLiteral(resourceName: "star"), for: .normal)
            if let id = FeatureViewController.chosenMovie.id{
                deleteData(id: id, entityname: "FavoriteList")
            }
            
        }
        
    }
    func saveRecord (entityname : String){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: entityname, in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        
        
        newUser.setValue(FeatureViewController.chosenMovie.tittle, forKey: "name")
        newUser.setValue(FeatureViewController.chosenMovie.id, forKey: "id")
        newUser.setValue(FeatureViewController.chosenMovie.image, forKey: "image")
        newUser.setValue(FeatureViewController.chosenMovie.release_date, forKey: "release_date")
        newUser.setValue(FeatureViewController.chosenMovie.vote_average, forKey: "vote_average")
        newUser.setValue(FeatureViewController.chosenMovie.vote_count, forKey: "vote_count")
        
        do {
            
            try context.save()
            
        } catch {
            
            print("Failed saving")
        }
    }
    func setTrailerKeyList() {
        
        if let id = FeatureViewController.chosenMovie.id{
            API.getJSONResponse(baseURL: API.getTrailerKeyURL(id: "\(id)"), params: API.getDefaultParam()){
                result in
                let results =  result["results"] as! NSArray
                //print(x)
                self.listTrailer.removeAll()
                for i in results{
                    
                    let t = i as! [String:Any]
                    if let item = TrailerDetail(JSON: t){
                        if let key = item.key{
                            self.listTrailer.append(key)
                        }
                        
                    }
                }
                
            }
        }
        
    }
    func setOverviewInfo(){
        if let id = FeatureViewController.chosenMovie.id {
            API.getJSONResponse(baseURL: API.getURLMovieDetail(id: "\(id)"), params: API.getDefaultParam()){ result in
                if let detail = MovieDetail(JSON: result){
                    FeatureViewController.chosenMovie = detail
                    if let released_date = FeatureViewController.chosenMovie.release_date{
                        self.overview.released_date.text = "Realeased date : \(released_date)"
                    }
                    
                    if let budget = FeatureViewController.chosenMovie.budget
                    {
                        self.overview.budget.text = "Budget : \(budget) $"
                    }
                    if let runtime = FeatureViewController.chosenMovie.runtime
                    {
                        self.overview.runtime.text = "Runtime : \(runtime) minutes"
                    }
                    if let revenue = FeatureViewController.chosenMovie.revenue
                    {
                        self.overview.revenue.text = "Revenue : \(revenue) $"
                    }
                    if let overview = FeatureViewController.chosenMovie.overview{
                        self.overview.plot.setAutoFitLabel(str: overview)
                    }
                }
                
                self.scrollView.contentSize = CGSize(width: self.parentView.frame.width, height: self.getContentHeight())
                
            }
            
        }
    }
    func setSimilarInfo(){
        if let id = FeatureViewController.chosenMovie.id {
            API.getJSONResponse(baseURL: API.getSimilarMoviesURL(id: "\(id)"), params: API.getParamMovieList(page: 1)){ result in
                let results =  result["results"] as! NSArray
                //print(x)
                self.listSimilarMovies1.removeAll()
                self.listSimilarMovies2.removeAll()
                var index = 0
                for i in results{
                    if index % 2 == 0{
                        let t = i as! [String:Any]
                        if let item = MovieDetail(JSON: t){
                            self.listSimilarMovies1.append(item)
                        }
                    }else {
                        let t = i as! [String:Any]
                        if let item = MovieDetail(JSON: t){
                            self.listSimilarMovies2.append(item)
                        }
                    }
                    index = index + 1
                }
                self.scrollView.contentSize = CGSize(width: self.parentView.frame.width, height: self.getContentHeight())
                
            }
        }
    }
    func setPeopleInfo(){
        if let id = FeatureViewController.chosenMovie.id {
            API.getJSONResponse(baseURL: API.getCreditsURL(id: "\(id)"), params: API.getDefaultParam()){ result in
                let castList = result["cast"] as! NSArray
                self.listCast.removeAll()
                for c in castList{
                    let cast = c as! [String:Any]
                    if let castItem = CastDetail(JSON: cast){
                        self.listCast.append(castItem)
                    }
                }
                self.listCrew.removeAll()
                let crewList = result["crew"] as! NSArray
                for c in crewList{
                    let crew = c as! [String:Any]
                    if let crewItem = CrewDetail(JSON: crew){
                        self.listCrew.append(crewItem)
                    }
                }
                self.scrollView.contentSize = CGSize(width: self.parentView.frame.width, height: self.getContentHeight())
                
            }
        }
    }
    func setMovieInfo(){
        if let id = FeatureViewController.chosenMovie.id {
            API.getJSONResponse(baseURL: API.getURLMovieDetail(id: "\(id)"), params: API.getDefaultParam()){ result in
                if let detail = MovieDetail(JSON: result){
                    FeatureViewController.chosenMovie = detail
                    if let vote_average = detail.vote_average
                    {
                        self.vote_average.setAutoFitLabel(str: "(\(vote_average))")
                        self.ratingController.setStarsRating(rating: Int(round(vote_average)))
                    }
                    if let homepage = detail.homepage{
                        self.homepage.setAutoFitLabel(str: homepage)
                        self.homepage.underline()
                    }
                    if let tittle = detail.tittle{
                        self.movieName.setAutoFitLabel(str: tittle)
                    }
                    if let release_date = detail.release_date{
                        self.release_date.setAutoFitLabel(str: release_date)
                    }
                    
                    
                    
                    if let image = detail.image{
                        self.setPosterImage(image: image)
                    }
                }
                
                let genres = result ["genres"] as! NSArray
                for genre in genres{
                    let t = genre as! [String: Any]
                    if let t = Genres(JSON: t){
                        if let name = t.name
                        {
                            self.genres.text = self.genres.text! + " \(name)"
                        }
                    }
                    
                    
                }
                self.genres.setAutoFitLabel(str: self.genres.text!) 
                self.scrollView.contentSize = CGSize(width: self.parentView.frame.width, height: self.getContentHeight())
            }
            
        }
    }
    func setPosterImage(image : String){
        let x = API.getFullURLOfImage(name: image)
        
        let Url = URL(string : x)
        //let data = try? Data(contentsOf: Url!) //
        //self.image.image = UIImage(data: data!)
        self.image.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "movie_placeholder-103642"))
    }
    
    @IBAction func redirect(_ sender: Any) {
        print(1)
        canGoBack = true
        if let link = FeatureViewController.chosenMovie.homepage{
            let url = URL(string: link)
            let vc = SFSafariViewController(url: url!)
            present(vc, animated: true, completion: nil)
            vc.delegate = self
        }
        
    }
    
    func getContentHeight() -> CGFloat {
        let x = 150 + self.movieName.frame.height + self.genres.frame.height + self.vote_average.frame.height + segment.frame.height
        return x + self.getSubViewHeight()
    }
    func getSubViewHeight() -> CGFloat{
        if segment.selectedSegmentIndex == 0{
            // 350 + 1 cục
            return 400 + self.overview.plot.frame.height
        }else if segment.selectedSegmentIndex == 1{
            return 485
        }else if segment.selectedSegmentIndex == 2 {
            return 375
        }
        return 0.0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        canGoBack = false
        setupLeftBarItem()
        setMovieInfo()
        
        if segment.selectedSegmentIndex == 0{
            addOverviewView()
        }else if segment.selectedSegmentIndex == 1{
            addPeopleSubview()
        }else if segment.selectedSegmentIndex == 2 {
            addSimilarSubview()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if canGoBack == false{
            super.viewWillDisappear(true)
            self.dismiss(animated: true, completion: nil)
            self.removeFromParent()

        }
    
    }
    
    
}
extension MovieDetailViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SFSafariViewControllerDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == overview.trailerCollectionView{
            return listTrailer.count
        }else if collectionView == people.castCollectionview{
            return listCast.count
        }
        else if collectionView == people.crewCollectionview{
            return listCrew.count
        }else if collectionView == similar.firstLineCollectionview
        {
            return listSimilarMovies1.count
        }
        else if collectionView == similar.secondLineCollectionview{
            return listSimilarMovies2.count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if segment.selectedSegmentIndex == 1 || segment.selectedSegmentIndex == 2 {
            guard let cell =  collectionView.cellForItem(at: indexPath) as? PosterCollectionViewCell else{
                return
            }
            if let image  = cell.image.image{
                FullscreenImage.imageFullScreen = image
//                let nextViewController = FullscreenImage()
//
//
//                navigationController?.pushViewController(nextViewController,animated: true)
                //dismiss(animated: true, completion: nil)
                canGoBack = true
                present(FullscreenImage(), animated: true, completion: nil)
            }
                
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if segment.selectedSegmentIndex == 0{
            return CGSize(width: 178, height: 149)
        }else if segment.selectedSegmentIndex == 1 {
            return CGSize(width: 65, height: 125)
        }else if segment.selectedSegmentIndex == 2 {
            return CGSize(width: 65, height: 125)
        }
        return CGSize(width: 65, height: 125)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if segment.selectedSegmentIndex == 0{
            let cell = overview.trailerCollectionView.dequeueReusableCell(withReuseIdentifier: "TrailerCollectionViewCell", for: indexPath) as! TrailerCollectionViewCell
            cell.backgroundColor = UIColor.black
            
            
            cell.getVideo(videoCode: listTrailer[indexPath.item])
            return cell
        }else if segment.selectedSegmentIndex == 1{
            if collectionView == people.castCollectionview{
                let cell  = people.castCollectionview.dequeueReusableCell(withReuseIdentifier: "PosterCollectionViewCell", for: indexPath) as! PosterCollectionViewCell
                cell.setCell(detail: listCast[indexPath.item])
                
                return cell
                
                
            }
            else if collectionView == people.crewCollectionview{
                let cell  = people.crewCollectionview.dequeueReusableCell(withReuseIdentifier: "PosterCollectionViewCell", for: indexPath) as! PosterCollectionViewCell
                cell.setCell(detail: listCrew[indexPath.item])
                
                
                return cell
                
                
            }
            
        } else if segment.selectedSegmentIndex == 2{
            if collectionView == similar.firstLineCollectionview{
                let cell = similar.firstLineCollectionview.dequeueReusableCell(withReuseIdentifier: "PosterCollectionViewCell", for: indexPath) as! PosterCollectionViewCell
                
                cell.backgroundColor = UIColor.black
                cell.setCell(detail: listSimilarMovies1[indexPath.item])
                
                return cell
            }
            else if collectionView == similar.secondLineCollectionview{
                let cell = similar.secondLineCollectionview.dequeueReusableCell(withReuseIdentifier: "PosterCollectionViewCell", for: indexPath) as! PosterCollectionViewCell
                
                cell.backgroundColor = UIColor.black
               
                cell.setCell(detail: listSimilarMovies2[indexPath.item])
                
                return cell
            }
            
        }
        
        return UICollectionViewCell()
        
    }
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
