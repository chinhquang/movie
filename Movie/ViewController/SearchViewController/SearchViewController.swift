//
//  SearchViewController.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/28/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController,UISearchBarDelegate {
    var pageIndex = 0
    var personSearchList : [PersonDetail] = [PersonDetail](){
        didSet{
            table.reloadData()
        }
    }
    var movieSearchList : [MovieDetail] = [MovieDetail](){
        didSet{
            table.reloadData()
        }
    }
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var table: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        setupView()
    }
    func setupView(){
        resetNavigationbar()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        UI.addDoneButton(controls: [searchBar])
        self.navigationItem.title = "Search"
        searchBar.delegate =  self
        searchBar.showsScopeBar = false
        
        table.register(UINib(nibName: "SearchResultCell", bundle: nil), forCellReuseIdentifier: "SearchResultCell")
        table.delegate = self
        table.dataSource = self
    }
    func resetNavigationbar() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        navigationController?.setNavigationBarHidden(false, animated: true)
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.showsScopeBar = false
        searchBar.endEditing(true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.movieSearchList.removeAll()
        self.personSearchList.removeAll()
        self.searchBar.text = ""
        self.searchBar.endEditing(true)
        
        self.dismiss(animated: true, completion: nil)
    }
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        navigationController?.setNavigationBarHidden(true, animated: true)
        searchBar.showsScopeBar = true
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        navigationController?.setNavigationBarHidden(false, animated: true)
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.showsScopeBar = false
        searchBar.endEditing(true)
        return true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.selectedScopeButtonIndex == 0{
            if searchText.count != 0{
                movieSearchList.removeAll()
                getMovieSearchList(query: searchText,pageIndex: 1)
            }else {
                movieSearchList.removeAll()
            }
        }else {
            if searchText.count != 0{
                personSearchList.removeAll()
                getPersonSearchList(query: searchText,pageIndex: 1)
            }else {
                personSearchList.removeAll()
            }
        }
    }
    // Load more
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        if searchBar.selectedScopeButtonIndex == 1{
            pageIndex = 1
            movieSearchList.removeAll()
            if let query = searchBar.text{
                if query.count != 0 {
                    getPersonSearchList(query: query,pageIndex: 1)
                }else {
                    personSearchList.removeAll()
                }
            }
            
        }else {
            pageIndex = 1
            personSearchList.removeAll()
            if let query = searchBar.text{
                if query.count != 0 {
                    getMovieSearchList(query: query,pageIndex: 1)
                }else {
                    movieSearchList.removeAll()
                }
            }
        }
    }
    func getMovieSearchList(query : String,pageIndex : Int){
        API.getJSONResponse(baseURL: API.baseURLSearchMovie, params: API.getParamSearchList(query: query, page: pageIndex)){
            result in
            let results =  result["results"] as! NSArray
            
            //self.movieSearchList.removeAll()
            for i in results{
                
                let t = i as! [String:Any]
                if let item = MovieDetail(JSON: t){
                    self.movieSearchList.append(item)
                }
            }
        }
    }
    func getPersonSearchList(query : String, pageIndex : Int){
        API.getJSONResponse(baseURL: API.baseURLSearchPeople, params: API.getParamSearchList(query: query, page: pageIndex)){
            result in
            let results =  result["results"] as! NSArray
            
            //self.personSearchList.removeAll()
            for i in results{
                
                let t = i as! [String:Any]
                if let item = PersonDetail(JSON: t){
                    self.personSearchList.append(item)
                }
            }
        }
    }
}
extension SearchViewController :UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBar.selectedScopeButtonIndex == 0{
            return movieSearchList.count
        }else if searchBar.selectedScopeButtonIndex == 1{
            return personSearchList.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.setNavigationBarHidden(false, animated: true)
        if searchBar.selectedScopeButtonIndex == 0{
            let nextViewController = MovieDetailViewController()
            FeatureViewController.chosenMovie = movieSearchList[indexPath.row]
            navigationController?.pushViewController(nextViewController,animated: true)
        }else {
            let nextViewController = PersonDetailViewController()
            FeatureViewController.chosenPerson = personSearchList[indexPath.row]
            navigationController?.pushViewController(nextViewController,animated: true)
        }
        
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if searchBar.selectedScopeButtonIndex == 0{
            let lastItem = movieSearchList.count - 1
            if indexPath.item == lastItem
            {
                pageIndex = pageIndex + 1
                if let searchText = self.searchBar.text {
                    getMovieSearchList(query: searchText, pageIndex: pageIndex)
                }
                
            }
        }
        else if searchBar.selectedScopeButtonIndex == 1{
            let lastItem = personSearchList.count - 1
            if indexPath.item == lastItem
            {
                pageIndex = pageIndex + 1
                if let searchText = self.searchBar.text {
                    getPersonSearchList(query: searchText, pageIndex: pageIndex)
                }

            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "SearchResultCell", for: indexPath) as! SearchResultCell
        cell.selectionStyle = .none
        if searchBar.selectedScopeButtonIndex == 0{
            if let tittle = movieSearchList[indexPath.row].tittle{
                cell.setCell(result: tittle)
            }
            
            return cell
        }else {
            if let tittle = personSearchList[indexPath.row].name{
                cell.setCell(result: tittle)
            }
            
            return cell
        }
    }
}
