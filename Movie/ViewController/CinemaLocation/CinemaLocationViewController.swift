//
//  CinemaLocationViewController.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/10/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import MapKit



class CinemaLocationViewController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate {
    static var cinemaDetail = CinemaDetail()
    var locationManager: CLLocationManager!
    var polyline : GMSPolyline = GMSPolyline()
    var GoogleMapsView: GMSMapView!
    var markerCurrent : GMSMarker = GMSMarker()
    var markerDest : GMSMarker = GMSMarker()
    var firstLoad : Bool = true
    
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var GoogleMapContainer: UIView!
    @IBOutlet weak var showMyLocation: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(true)
        self.removeFromParent()
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func showCurrent(_ sender: Any) {
        let camera = GMSCameraPosition.camera(withLatitude: markerCurrent.position.latitude, longitude: markerCurrent.position.longitude, zoom: 16)
        self.GoogleMapsView.camera = camera

    }
    @IBAction func drawDirection(_ sender: Any) {
        self.draw(src: markerCurrent.position, dst: markerDest.position){
            result in
        }
    }
    @IBAction func segmentValueChange(_ sender: UISegmentedControl) {
        setMode()
    }

    func setMode() -> Void {
        do {
            var style : String?
            if segment.selectedSegmentIndex == 0{
                style = "dark_style"
            }else{
                style = "light_style"
            }
            if let styleURL = Bundle.main.url(forResource: style, withExtension: "json") {
                GoogleMapsView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        setNavigationbar()
        
        self.GoogleMapsView = GMSMapView(frame: self.GoogleMapContainer.bounds)
        self.GoogleMapContainer.addSubview(self.GoogleMapsView)
        self.GoogleMapsView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.GoogleMapContainer.layoutIfNeeded()
        setMode()
        //self.showMyLocation.setImage(#imageLiteral(resourceName: "button_my_location"), for: .normal)
        view.layoutIfNeeded()
        GoogleMapsView.delegate = self
        setDest()
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        
    }
    func setDest() -> Void {
        if let position = CinemaLocationViewController.cinemaDetail.cine_position?.toCoordinate(){
//            print(position.latitude)
//            print(position.longitude)
            self.markerDest = GMSMarker(position: position)
            self.markerDest.title = CinemaLocationViewController.cinemaDetail.cinema_address
            self.markerDest.map = self.GoogleMapsView
            let camera = GMSCameraPosition.camera(withLatitude: position.latitude, longitude: position.longitude, zoom: 16)
            GoogleMapsView.camera = camera
        }
        
    }
    func setNavigationbar() -> Void {
        self.resetNavigationBar()
        self.navigationItem.title = CinemaLocationViewController.cinemaDetail.cinema_abb
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        markerCurrent.map = nil
        let location = locations.last! as CLLocation
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        //let position = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        markerCurrent = GMSMarker(position: center)
        
        
        
        
        markerCurrent.icon = #imageLiteral(resourceName: "icons8-street-view-96")
        
        markerCurrent.map = self.GoogleMapsView
        
    }

    func draw(src: CLLocationCoordinate2D, dst: CLLocationCoordinate2D, completion:  @escaping ([String:Any]) -> Void){
       
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        guard let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(src.latitude),\(src.longitude)&destination=\(dst.latitude),\(dst.longitude)&sensor=false&mode=driving&key=AIzaSyCOPRYcMvUCduB1IP-yuy6oXVokQZpMjl0")else {
            return
        }
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            guard let data = data else { return  }
            if error != nil {
                print(error!.localizedDescription)
            } else {
                
                
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] {
                        //-----------------------------------------------
                        let preRoutes = json["routes"] as! NSArray
                        let routes = preRoutes[0] as! NSDictionary
                        let routeOverviewPolyline:NSDictionary = routes.value(forKey: "overview_polyline") as! NSDictionary
                        let polyString = routeOverviewPolyline.object(forKey: "points") as! String
                        
                        //-----------------------------------------------
                        let legs = routes["legs"] as! NSArray
                        let legsParse = legs[0] as! NSDictionary
                        
                        
                        //----------steps to take to dest point----------
                        let steps : NSArray = legsParse.value(forKey: "steps") as! NSArray
                        var i = 0
                        var distanceSum = 0
                        var durationSum = 0
                        while (i < steps.count){
                            let subStep : NSDictionary = steps[i] as! NSDictionary
                            //get distance
                            let distance :NSDictionary = subStep["distance"] as! NSDictionary
                            let dicstanceVal : Int = distance["value"] as! Int
                            distanceSum = distanceSum + dicstanceVal
                            
                            //get duration
                            let duration :NSDictionary = subStep["duration"] as! NSDictionary
                            let durationVal : Int = duration["value"] as! Int
                            durationSum = durationSum + durationVal
                            i = i + 1
                        }
                        
                        
                        let x = Double(Double(distanceSum) / 1000).rounded(toPlaces: 1)
                        
                        
                        DispatchQueue.main.async(execute: {
                            self.polyline.map = nil
                            let path = GMSPath(fromEncodedPath: polyString)
                            self.polyline = GMSPolyline(path: path)
                            self.polyline.strokeWidth = 5.0
                            self.polyline.strokeColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
                            self.polyline.geodesic = true
                            self.polyline.map = self.GoogleMapsView
                            self.GoogleMapsView.animate(with: GMSCameraUpdate.fit(GMSCoordinateBounds(path: self.polyline.path!), withPadding: 30))
                            if let name =  CinemaLocationViewController.cinemaDetail.cinema_abb{
                                self.navigationItem.title = name + " (\(x)km)"
                            }
                        })
                    }
                    
                } catch {
                    print("parsing error")
                    
                }
            }
        })
        task.resume()
    }
    

}
