//
//  TopRatedViewController.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/30/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
import CoreData

class TopRatedViewController: UIViewController {
    @IBOutlet var btnMenu: [UIButton]!
    var list : [MovieDetail] = [MovieDetail](){
        didSet{
            table.reloadData()
        }
    }
    var index = 0
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(true)
        self.index = 0
        
        self.dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        self.resetNavigationBar()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        // Do any additional setup after loading the view.
        table.register(UINib(nibName: "MovieDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "MovieDetailTableViewCell")
        table.delegate = self
        table.dataSource = self
        table.tableFooterView = UIView()
        setupNavigationBarItem()
        //        print(fetchData(entityname: "WatchList")[0].image as Any)
        setFavoriteListValue()
        tapOutSide()
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapOutSide))
//
//        view.addGestureRecognizer(tap)
    }
    
    
    @IBAction func sortByRate(_ sender: Any) {
        list = Sort.sortByRate(list: list)
        tapOutSide()
    }
    @IBAction func sortByDate(_ sender: Any) {
        list = Sort.sortByDate(list: list)
        tapOutSide()
    }
    @IBAction func sortByName(_ sender: Any) {
        list = Sort.sortByName(list: list)
        tapOutSide()
    }
    func setWatchListValue(){
        list.removeAll()
        for i in fetchData(entityname: "WatchList"){
            list.append(i)
        }
    }
    func setFavoriteListValue()  {
        list.removeAll()
        for i in fetchData(entityname: "FavoriteList"){
            list.append(i)
        }
    }
    func setRateListValue()  {
        list.removeAll()
        for i in fetchData(entityname: "RateList"){
            list.append(i)
        }
    }
    func setupNavigationBarItem(){
        // left bar
        let items = ["Favorite", "Watchlist","Rate"]
        let filtersSegment = UISegmentedControl(items: items)
        
        filtersSegment.selectedSegmentIndex = 0
        index = 0
        filtersSegment.tintColor = UIColor.orange
        filtersSegment.addTarget(self, action: #selector(self.filterApply), for: UIControl.Event.valueChanged)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: filtersSegment)
        
        
        // right bar
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit
            , target: self, action: #selector(self.editListBySorting
            ))
        
        
    }
    @objc func filterApply(segment: UISegmentedControl) -> Void {
        if segment.selectedSegmentIndex == 0{
            setFavoriteListValue()
        }else if segment.selectedSegmentIndex == 1{
            setWatchListValue()
            index = 1
        }else if segment.selectedSegmentIndex == 2{
            setRateListValue()
            index = 2
        }
    }
    
    @objc func editListBySorting() -> Void{
        btnMenu.forEach{(button) in
            button.isHidden = !button.isHidden
            
        }
    }
    
    @objc func tapOutSide() -> Void{
        btnMenu.forEach{(button) in
            if button.isHidden != true {
                button.isHidden = true
            }
            
        }
    }
    @objc func watchList (sender : UIButton){
        
        FeatureViewController.chosenMovie = list[sender.tag]
        navigationController?.pushViewController(TrailerListViewController(), animated: true)
    }
    func fetchData (entityname : String)-> [MovieDetail]{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        //let entity = NSEntityDescription.entity(forEntityName: "EmployeeData", in: context)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityname)
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        var cloneArray : [MovieDetail] = [MovieDetail]()
        do {
            let result = try context.fetch(request)
            if let results = result as? [NSManagedObject]{
                for data in results  {
                    
                    let detail = MovieDetail()
                    if let t = data.value(forKey : "name") as? String{
                        detail.tittle = t
                    }
                    if let t = data.value(forKey : "id") as? Int{
                        detail.id = t
                    }
                    if let t = data.value(forKey : "release_date") as? String{
                        detail.release_date = t
                    }
                    if let t = data.value(forKey : "image") as? String{
                        detail.image = t
                    }
                    if let t = data.value(forKey : "vote_count") as? Int{
                        detail.vote_count = t
                    }
                    if let t = data.value(forKey : "vote_average") as? Double{
                        detail.vote_average = t
                    }
                    
                    cloneArray.append(detail)
                }
            }
            
            
            
        } catch {
            
            
            print("Failed fetching data")
        }
        return cloneArray
    }
    func deleteData(id: Int, entityname : String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityname)
        fetchRequest.predicate = NSPredicate.init(format: "id = \(id)")
        
        
        
        do {
            let results = try context.fetch(fetchRequest)
            if results.count > 0{
                for x in results {
                    let objectDelete = x as NSManagedObject
                    context.delete(objectDelete)
                    do{
                        try context.save()
                    }catch{
                        print(error)
                    }
                }
                
                
            }
            
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        
    }
    
}
extension TopRatedViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = table.dequeueReusableCell(withIdentifier: "MovieDetailTableViewCell", for: indexPath) as! MovieDetailTableViewCell
        cell.selectionStyle = .none
        cell.setCell(detail: list[indexPath.row])
        cell.watchTrailer.tag = indexPath.row
        cell.watchTrailer.addTarget(self, action: #selector(watchList(sender:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextViewController = MovieDetailViewController()
        FeatureViewController.chosenMovie = list[indexPath.row]
        navigationController?.pushViewController(nextViewController,animated: true)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
        
            if let id = list[indexPath.row].id{
                print(id)
                print(index)
                self.list.remove(at: indexPath.row);
                if index == 0 {
                    deleteData(id: id, entityname: "FavoriteList")
                }else if index == 1{
                    deleteData(id: id, entityname: "WatchList")
                }else if index == 2{
                    deleteData(id: id, entityname: "RateList")
                }
                
            }
            
            
            
        }
    

    }
    
}
