//
//  CinemaViewController.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/31/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
import ObjectMapper
class CinemaViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var isShow : Bool = false {
        didSet{
            UIView.animate(withDuration: 0.2) {
                if (self.isShow == true){
                    self.listHeight.constant = 200
                    self.view.layoutIfNeeded()
                    return
                }
                self.listHeight.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    var cityList : [String] = [String](){
        
        didSet{
            optionsTable.reloadData()
        }
    }
    var cinemaList : [CinemaDetail] = [CinemaDetail](){
        didSet{
            cinemaTable.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == optionsTable{
            return cityList.count
        }
        if tableView == cinemaTable{
            return cinemaList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == optionsTable{
            if let cell = optionsTable.dequeueReusableCell(withIdentifier: "SearchResultCell", for: indexPath) as? SearchResultCell{
               
                
                cell.setCell(result: cityList[indexPath.row])
                return cell
            }
            
            
        }else if tableView == cinemaTable{
            if let cell = cinemaTable.dequeueReusableCell(withIdentifier: "TheatreDetailTableViewCell", for: indexPath) as? TheatreDetailTableViewCell{
                
                if cinemaList.count != 0{
                    cell.setCell(detail: cinemaList[indexPath.row])
                    return cell
                }
                
            }
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == optionsTable{
            print(indexPath.row)
            self.textField.text = cityList[indexPath.row]
            
            isShow = false
            getListCinema(id: indexPath.row)
        }else if tableView == cinemaTable{
            let nextViewController = CinemaLocationViewController()
            
           CinemaLocationViewController.cinemaDetail = cinemaList[indexPath.row]
            navigationController?.pushViewController(nextViewController,animated: true)
            
        }
        
    }

    
    
    
    @IBOutlet weak var listHeight: NSLayoutConstraint!
    @IBOutlet weak var optionsTable: UITableView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var cinemaTable: UITableView!
    @IBAction func showAll(_ sender: Any) {
        isShow = !isShow
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(true)
        setNavigationbar()
        initView()
        cityList = ["Hà Nội","Sài Gòn","Đồng Nai","Đà Nẵng","Cần Thơ"]
        
        cinemaTable.register(UINib(nibName: "TheatreDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "TheatreDetailTableViewCell")
        cinemaTable.delegate = self
        cinemaTable.dataSource = self
        
    }
    func setNavigationbar() -> Void {
        self.resetNavigationBar()
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        self.navigationItem.title = "Cinema"
    }
    func initView() -> Void {
        self.textField.isUserInteractionEnabled = false
        self.listHeight.constant = 0
        self.view.layoutIfNeeded()
        optionsTable.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        optionsTable.layer.cornerRadius = 4
        optionsTable.layer.borderWidth = 1
        optionsTable.register(UINib(nibName: "SearchResultCell", bundle: nil), forCellReuseIdentifier: "SearchResultCell")
        optionsTable.delegate = self
        optionsTable.dataSource = self
        self.listHeight.constant = 0
        self.view.layoutIfNeeded()
        textField.text = "Hà Nội"
        getListCinema(id: 0)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.dismiss(animated: true, completion: nil)
    }
    func getListCinema (id : Int) {
        cinemaList.removeAll()
        var x : String? = ""
        if id  == 0 {
            x = "hn.json"
        }
        else if id  == 1 {
            x = "sg.json"
        }
        else if id  == 2 {
            x = "dongnai.json"
        }
        else if id  == 3{
            x = "danang.json"
        }
        else if id  == 4 {
            x = "ct.json"
        }
        if let city = x{
            let url = "\(API.listCinemaURL)\(city)"
            
            API.getJSONResponse(baseURL: url, params: [:]){
                result in
                //print(result)
                let results =  result["data"] as! NSArray
                //print(x)
                self.cinemaList.removeAll()
                for i in results{
                    
                    let t = i as! [String:Any]
                    if let item = CinemaDetail(JSON : t){
                        self.cinemaList.append(item)
                        
                    }
                    
                }
            }
        }
        
    }

}
