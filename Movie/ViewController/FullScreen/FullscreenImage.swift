//
//  FullscreenImage.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/14/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class FullscreenImage: UIViewController,UIGestureRecognizerDelegate {
    static var imageFullScreen = UIImage()
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var close_button: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        close_button.isHidden = true
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.recognizer.cancelsTouchesInView = false
        
        self.recognizer.delegate = self
        
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        ImageView.image = FullscreenImage.imageFullScreen
        
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        // Get the location in CGPoint
        let location = touch.location(in: nil)
        
        // Check if location is inside the view to avoid
        if close_button.frame.contains(location) {
            return false
        }
        
        return true
    }
    @IBOutlet var recognizer: UITapGestureRecognizer!
    
    @IBAction func close(_ sender: Any) {
        print(1)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func tapScreen(_ sender: UIGestureRecognizer) {
        
        isHiden = !isHiden
    }
    var isHiden : Bool = true {
        didSet{
            UIView.transition(with: close_button, duration: 0.5, options: .transitionCrossDissolve, animations: {
                if self.isHiden == true{
                    self.close_button.isHidden = true
                }else{
                    self.close_button.isHidden = false
                }
               
            })
        }
    }
    
    
}
