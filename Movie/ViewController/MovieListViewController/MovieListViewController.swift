//
//  MovieListViewController.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/1/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
class MovieListViewController: UIViewController{
    static var id : Int?
    var pageIndex : Int = 0
    var canGoBack = false
    var list : [MovieDetail] = [MovieDetail](){
        didSet{
            listAll.reloadData()
        }
    }
    
    var refreshControl : UIRefreshControl = UIRefreshControl()
    @IBOutlet weak var listAll: UITableView!
    override func viewWillAppear(_ animated: Bool) {
        canGoBack = false
        super.viewWillAppear(true)
        self.listAll.tableFooterView = UIView()
        setupView()
        AddPullToRefresh()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       
        if canGoBack == false{
            super.viewWillDisappear(true)
            self.dismiss(animated: true, completion: nil)
            self.removeFromParent()
            
        }
        
    }
    
    func AddPullToRefresh (){
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: attributes)
        
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        listAll.addSubview(refreshControl) // not required when using UITableViewController
    }
    @objc func refresh() {
        pageIndex = 0
        list.removeAll()
        loadMoreData()
        refreshControl.endRefreshing();
    }
    func setupView() {
        self.navigationItem.title = "List Movie"
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        
        listAll.register(UINib(nibName: "MovieDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "MovieDetailTableViewCell")
        listAll.delegate = self
        listAll.dataSource = self
        loadMoreData()
    }
    func setTopRatedMovieList(pageIndex : Int) {
        API.getJSONResponse(baseURL: API.baseURLTopRatedMovies, params: API.getParamMovieList(page: pageIndex)){ result in
            
            let results =  result["results"] as! NSArray
            
//            self.list.removeAll()
            for i in results{
                
                let t = i as! [String:Any]
                if let item = MovieDetail(JSON: t){
                    self.list.append(item)
                }
                
                
                
            }
            
            
        }
    }
    
    func setPopularMovieList(pageIndex : Int) {
        API.getJSONResponse(baseURL: API.baseURLPopularMovies, params: API.getParamMovieList(page: pageIndex)){ result in
            
            let results =  result["results"] as! NSArray
            //print(x)
//            self.list.removeAll()
            for i in results{
                
                let t = i as! [String:Any]
                
                if let item = MovieDetail(JSON: t){
                    self.list.append(item)
                }
            }
            
        }
    }
    func setNowPlayingMovieList(pageIndex : Int) {
        API.getJSONResponse(baseURL: API.baseURLNowPlayingMovies, params: API.getParamMovieList(page: 1)){ result in
            
            let results =  result["results"] as! NSArray
            //print(x)
//            self.list.removeAll()
            for i in results{
                
                let t = i as! [String:Any]
                if let item = MovieDetail(JSON: t){
                    self.list.append(item)
                }
            }
        }
    }
    
    @objc func watchList (sender : UIButton){
        canGoBack = true
        FeatureViewController.chosenMovie = list[sender.tag]
        navigationController?.pushViewController(TrailerListViewController(), animated: true)
    }

}

extension MovieListViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.2) {
            if let cell = tableView.cellForRow(at: indexPath) as? MovieDetailTableViewCell {
                cell.transform = .init(scaleX: 0.95, y: 0.95)
                cell.contentView.backgroundColor = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
            }
        }
    }
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.2) {
            if let cell = tableView.cellForRow(at: indexPath) as? MovieDetailTableViewCell {
                cell.transform = .identity
                cell.contentView.backgroundColor = .clear
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = listAll.dequeueReusableCell(withIdentifier: "MovieDetailTableViewCell", for: indexPath) as! MovieDetailTableViewCell
        cell.selectionStyle = .none
        cell.setCell(detail: list[indexPath.row])
        cell.watchTrailer.tag = indexPath.row
        cell.watchTrailer.addTarget(self, action: #selector(watchList(sender:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = list.count - 1
        if indexPath.item == lastItem
        {
            loadMoreData()
        }
    }
    func loadMoreData() {
        pageIndex = pageIndex + 1
        if let id  = MovieListViewController.id{
            if id == 0{
                setTopRatedMovieList(pageIndex: pageIndex)
            }else if id == 1{
                setPopularMovieList(pageIndex: pageIndex)
            }else if id  == 3 {
                setNowPlayingMovieList(pageIndex: pageIndex)
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextViewController = MovieDetailViewController()
        FeatureViewController.chosenMovie = list[indexPath.row]
        canGoBack = true
        navigationController?.pushViewController(nextViewController,animated: true)
    }
}
