//
//  PersonDetailViewController.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/2/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class PersonDetailViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    var biography = BiographySubview()
    var credit = CreditSubview()
    var checkFirst = 0
    var canGoBack : Bool = false
    var listCrew : [MovieDetail] = [MovieDetail](){
        didSet{
            credit.crewCollectionView.reloadData()
        }
    }
    var listCast : [MovieDetail] = [MovieDetail](){
        didSet{
            credit.castCollectionView.reloadData()
        }
    }
    
    @IBOutlet weak var separator: UIView!
    @IBOutlet weak var known_for_department: UILabel!
    @IBOutlet weak var place_of_birth: UILabel!
    @IBOutlet weak var birthday: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var subviewLoading: UIView!
    @IBOutlet weak var seg: UISegmentedControl!
    @IBOutlet weak var parentView: UIView!
    @IBAction func segmentClick(_ sender: UISegmentedControl) {
        if seg.selectedSegmentIndex == 0{
            addBiographySubview()
        }else if seg.selectedSegmentIndex == 1{
            addCreditSubview()
        }
        self.view.layoutIfNeeded()
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        setPersonInfo()
        canGoBack = false
        
        
    }
    
    func getContentHeight() -> CGFloat{
        let x = self.name.frame.height + self.birthday.frame.height + self.seg.frame.height + self.place_of_birth.frame.height + self.known_for_department.frame.height
        
        
       return x + getSubviewHeight()
    }
    func getSubviewHeight()->CGFloat{
        if seg.selectedSegmentIndex == 0{
            return biography.overviewHistory.frame.height + 70
        }else if seg.selectedSegmentIndex == 1 {
            return 600
            
        }
        return 0.0
    }
    func addCreditSubview() {
        for subUIView in subviewLoading.subviews as [UIView] {
            subUIView.removeFromSuperview()
        }
        
        
        
        credit = CreditSubview()
        credit.awakeFromNib()
        
        subviewLoading.addSubview(credit)
    
        credit.castCollectionView.delegate = self
        credit.castCollectionView.dataSource = self
        
        credit.crewCollectionView.delegate = self
        credit.crewCollectionView.dataSource = self
        
        credit.castCollectionView.register(UINib(nibName: "PosterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PosterCollectionViewCell")
        
        credit.crewCollectionView.register(UINib(nibName: "PosterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PosterCollectionViewCell")
        
        subviewLoading.topAnchor.constraint(equalTo: separator.bottomAnchor, constant: 33.5).isActive = true
        subviewLoading.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant: 0 ).isActive = true
        subviewLoading.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: 0).isActive = true
        subviewLoading.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: 0).isActive = true
        credit.frame =  subviewLoading.bounds
        credit.autoresizingMask = [.flexibleWidth, .flexibleHeight]
       
        setCreditInfo()
       
    }
    func setCreditInfo(){
        if let id = FeatureViewController.chosenPerson.id{
            API.getJSONResponse(baseURL: API.getURLMovieCredit(id: "\(id)"), params: API.getDefaultParam()){
                result in
                let casts = result["cast"] as! NSArray
                self.listCast.removeAll()
                
                for i in casts{
                    
                    let t = i as! [String:Any]
                    if let item = MovieDetail(JSON: t){
                        self.listCast.append(item)
                    }
                }
                let crews = result["crew"] as! NSArray
                self.listCrew.removeAll()
                
                for i in crews{
                    
                    let t = i as! [String:Any]
                    if let item = MovieDetail(JSON: t){
                        self.listCrew.append(item)
                    }
                }
                print(3)
                self.setContentSizeScrollview()
            }
        }
    }
    func setContentSizeScrollview(){
        self.scrollView.contentSize = CGSize(width: self.parentView.frame.width, height: self.getContentHeight())
    }
    func addBiographySubview(){
        for subUIView in subviewLoading.subviews as [UIView] {
            subUIView.removeFromSuperview()
        }
        biography = BiographySubview()
        biography.awakeFromNib()
        subviewLoading.addSubview(biography)
        
        subviewLoading.topAnchor.constraint(equalTo: separator.bottomAnchor, constant: 33.5).isActive = true
        subviewLoading.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant: 0 ).isActive = true
        subviewLoading.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: 0).isActive = true
        subviewLoading.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: 0).isActive = true
        biography.frame =  subviewLoading.bounds
        biography.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        setBiographyInfo()
        
        
    }
    func setBiographyInfo(){
        
        if let id = FeatureViewController.chosenPerson.id{
            API.getJSONResponse(baseURL: API.getURLPersonDetail(id: "\(id)"), params: API.getDefaultParam()){
                result in
                
                if let detail = PersonDetail(JSON: result){
                    
                    if let overview = detail.biography
                    {
                        
                        self.biography.overviewHistory.setAutoFitLabel(str: overview)
                        print(2)
                        self.setContentSizeScrollview()
                    }
                    
                }
                
                
            }
        }
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        setContentSizeScrollview()
    }
    func setPersonInfo(){
        
        if let id = FeatureViewController.chosenPerson.id{
            API.getJSONResponse(baseURL: API.getURLPersonDetail(id: "\(id)"), params: API.getDefaultParam()){
                result in
                print(1)
                if let detail = PersonDetail(JSON: result){
                    
                    if let name = detail.name
                    {
                        self.name.setAutoFitLabel(str: name)
                        
                    }
                    if let birthday = detail.birthday{
                        self.birthday.setAutoFitLabel(str: birthday)
                       
                    }
                    
                    if let place_of_birth = detail.place_of_birth{
                        self.place_of_birth.setAutoFitLabel(str: place_of_birth)
                       
                    }
                    
                    if let knowfordepartment =  detail.known_for_department
                    {
                        self.known_for_department.setAutoFitLabel(str: "Known for department: \(knowfordepartment)")
                    }
                    if let image = detail.profile_path{
                        self.setPosterImage(image: image)
                        
                    }
                   
                    
                    
                }
                self.addBiographySubview()
                self.setContentSizeScrollview()
                
            }
            
        }
        
    }
    func setPosterImage(image : String){
        let x = API.getFullURLOfImage(name: image)
        
        let Url = URL(string : x)
        //let data = try? Data(contentsOf: Url!) //
        //self.image.image = UIImage(data: data!)
        self.avatar.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "avatar_placeholder"))
    }
    override func viewWillDisappear(_ animated: Bool) {
        if canGoBack == false{
            super.viewWillDisappear(animated
            )
            self.removeFromParent()
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
}
extension PersonDetailViewController : UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == credit.castCollectionView{
            return listCast.count
        }else if collectionView == credit.crewCollectionView{
            return listCrew.count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width: 95, height: 160)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == credit.castCollectionView{
            let cell = credit.castCollectionView.dequeueReusableCell(withReuseIdentifier: "PosterCollectionViewCell", for: indexPath) as! PosterCollectionViewCell
            cell.setCell(detail: listCast[indexPath.item])
            return cell
        }else if collectionView == credit.crewCollectionView{
            let cell = credit.crewCollectionView.dequeueReusableCell(withReuseIdentifier: "PosterCollectionViewCell", for: indexPath) as! PosterCollectionViewCell
            cell.setCell(detail: listCrew[indexPath.item])
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if seg.selectedSegmentIndex == 1 {
            guard let cell =  collectionView.cellForItem(at: indexPath) as? PosterCollectionViewCell else{
                return
            }
            if let image  = cell.image.image{
                FullscreenImage.imageFullScreen = image
                canGoBack = true
                present(FullscreenImage(), animated: true, completion: nil)
            }
            
            
        }
    }
    
    
    
}
