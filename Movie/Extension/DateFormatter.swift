//
//  DateFormatter.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/4/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation

extension String{
    func convertToDate() -> Date {
        let dateformat = DateFormatter()
        dateformat.dateFormat = "yyyy-MM-dd"
        if let date = dateformat.date(from: self)
        {
            return date
        }
        return Date()
    }
}
