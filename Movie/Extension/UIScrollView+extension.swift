//
//  UIScrollView+extension.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/15/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit
extension UIScrollView{
    func updateContentSize() -> Void {
        self.contentSize = self.subviews.reduce(CGRect(), { $0.union($1.frame) }).size
    }
}
