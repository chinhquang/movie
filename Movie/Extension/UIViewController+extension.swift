//
//  UIViewController+extension.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/7/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController{
    func resetNavigationBar(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
}
