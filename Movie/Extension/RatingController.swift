//
//  RatingController.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/31/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit

class RatingController: UIStackView {
    var starsRating = 0
    
    override func draw(_ rect: CGRect) {
        let starButtons = self.subviews.filter{$0 is UIButton}
        var starTag = 1
        for button in starButtons {
            if let button = button as? UIButton{
                button.setImage(#imageLiteral(resourceName: "star"), for: .normal)
                //button.addTarget(self, action: #selector(self.pressed(sender:)), for: .touchUpInside)
                button.tag = starTag
                starTag = starTag + 1
            }
        }
        setStarsRating(rating:starsRating)
    }
    func setStarsRating(rating:Int){
        self.starsRating = rating
        let stackSubViews = self.subviews.filter{$0 is UIButton}
        for subView in stackSubViews {
            if let button = subView as? UIButton{
                if button.tag > starsRating {
                    button.setImage(#imageLiteral(resourceName: "star"), for: .normal)
                }else{
                    button.setImage(#imageLiteral(resourceName: "starfill"), for: .normal)
                }
            }
        }
    }
    @objc func pressed(sender: UIButton) {
        setStarsRating(rating: sender.tag)
    }
}
