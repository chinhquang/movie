//
//  UIString+extension.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/11/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation

import UIKit
import CoreLocation

extension String{
    
    func toCoordinate() ->  CLLocationCoordinate2D {
        
        var array =  self.components(separatedBy: ",")
        let location:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: Double((array[0] as NSString).doubleValue), longitude: Double((array[1] as NSString).doubleValue))
        return location
        
    }
}
