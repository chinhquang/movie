//
//  Sort.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/4/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit
class Sort{
    
    static func sortByName(list : [MovieDetail])->[MovieDetail]{
        let listsorted = list.sorted(by: {
            $0.tittle! < $1.tittle!
        })
        return listsorted
    }
    static func sortByRate(list : [MovieDetail])->[MovieDetail]{
        let listsorted = list.sorted(by: {
            $0.vote_average! < $1.vote_average!
        })
        return listsorted
    }
    static func sortByDate(list : [MovieDetail])->[MovieDetail]{
        
        let listsorted = list.sorted(by: {
            $0.release_date!.convertToDate().timeIntervalSinceNow < $1.release_date!.convertToDate().timeIntervalSinceNow
        })
        return listsorted
    }
}
