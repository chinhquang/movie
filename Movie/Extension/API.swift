//
//  API.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/28/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//
import Foundation
import Alamofire
class API {
    public static var key = "c9238e9fff997ddc12fc76e3904e2618"
    public static var baseURL = "https://api.themoviedb.org/3"
    public static var baseURLPopularMovies = API.baseURL + "/movie/popular"
    public static var baseURLTopRatedMovies = API.baseURL + "/movie/top_rated"
    public static var baseURLNowPlayingMovies = API.baseURL + "/movie/now_playing"
    public static var imageURL = "https://image.tmdb.org/t/p/w1280"
    public static var baseURLSearchMovie = API.baseURL + "/search/movie"
    public static var baseURLSearchPeople = API.baseURL + "/search/person"
    public static var baseURLPopularPeople = API.baseURL + "/person/popular"
    public static var logoURL = "http://lichphim.vn/assets/images/images/"
    public static func getFullURLOfImage(name : String)->String{
        return imageURL + "\(name)"
    }
    static func getParamMovieList(page : Int) ->[String:Any]{
        return ["api_key": "\(API.key)", "page": "\(page)"]
    }
    static func getParamSearchList(query : String ,page : Int) ->[String:Any]{
        return ["api_key": "\(API.key)", "page": "\(page)","query":"\(query)"]
    }
    static func getURLMovieDetail (id : String)->String{
        return baseURL + "/movie/" + id
    }
    static func getDefaultParam() ->[String:Any]{
        return ["api_key": "\(API.key)"]
    }
    static func getJSONResponse (baseURL: String,params : [String:Any],completion:  @escaping ([String:Any]) -> Void){
        
        Alamofire.request(baseURL, method: .get, parameters: params)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
            
                if let json = response.result.value as? [String: Any]{
                    completion(json)
                }
                
                
        }
        
    }
    static var listCinemaURL = "https://iosmoviedb.herokuapp.com/listcinema/"
    static func getTrailerKeyURL (id : String)->String{
        return baseURL + "/movie/" + id + "/videos"
        
    }
    static func getCreditsURL (id : String)->String{
        return baseURL + "/movie/" + id + "/credits"
        
    }
    static func getSimilarMoviesURL (id : String)->String{
        return baseURL + "/movie/" + id + "/similar"
        
    }
    static func getURLPersonDetail(id : String)->String{
        return baseURL + "/person/" + id
    }
    static func getURLMovieCredit(id :String)->String{
        return baseURL + "/person/\(id)/movie_credits"
    }
    
}

