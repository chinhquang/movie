//
//  CrewDetail.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/30/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import Foundation
import ObjectMapper
class CrewDetail : Mappable{
    var credit_id :  String?
    var department : String?
    var gender : Int?
    var id : Int?
    var job : String?
    var name : String?
    var profile_path : String?
    
    
    required init?(map: Map){
        
    }
    init() {
        
    }
    func mapping(map: Map) {
        credit_id <- map["cast_id"]
        department <- map["department"]
        
        id <- map["id"]
        gender <- map["gender"]
        name <- map["name"]
        job <- map["job"]
        profile_path <- map["profile_path"]
        
        
    }
}
