//
//  Genres.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/30/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import Foundation
import ObjectMapper
class Genres: Mappable{
    
    var id  : Int?
    var name : String?
    
    required init?(map: Map){
        
    }
    init() {
        
    }
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
    }
}

