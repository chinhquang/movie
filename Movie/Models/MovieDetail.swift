//
//  MovieDetail.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/28/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//
import Foundation
import ObjectMapper
class MovieDetail: Mappable{
    var image : String?
    var tittle : String?
    var release_date : String?
    var id  : Int?
    var vote_average : Double?
    var overview : String?
    var budget : Int?
    var homepage : String?
    var revenue : Int?
    var runtime : Int?
    var backdrop_path : String?
    var vote_count : Int?
    required init?(map: Map){
        
    }
    init() {
        
    }
    func mapping(map: Map) {
        vote_count <- map["vote_count"]
        image <- map["poster_path"]
        tittle <- map["title"]
        release_date <- map["release_date"]
        id <- map["id"]
        vote_average <- map["vote_average"]
        overview <- map["overview"]
        revenue <- map["revenue"]
        runtime <- map["runtime"]
        budget <- map["budget"]
        homepage <- map["homepage"]
        backdrop_path <- map["backdrop_path"]
    }
}

