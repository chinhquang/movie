//
//  CastDetail.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/30/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import Foundation
import ObjectMapper
class CastDetail : Mappable{
    var cast_id : Int?
    var character : String?
    var credit_id : String?
    var gender : Int?
    var id : Int?
    var name : String?
    var order : Int?
    var profile_path : String?
    
    
    required init?(map: Map){
        
    }
    init() {
        
    }
    func mapping(map: Map) {
        cast_id <- map["cast_id"]
        character <- map["character"]
        credit_id <- map["credit_id"]
        id <- map["id"]
        gender <- map["gender"]
        name <- map["name"]
        order <- map["order"]
        profile_path <- map["profile_path"]
        
        
    }
}


