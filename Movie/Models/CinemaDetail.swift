//
//  CinemaDetail.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/10/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import ObjectMapper
class CinemaDetail : Mappable{
    var cinema_id : Int?
    var cinema_name : String?
    
    var cinema_image: String?
    var cinema_rate : Int?
    var cine_position : String?
    var cinema_address : String?
    var cinema_abb : String?
    var cinema_link_book :String?
    var cinema_phone : String?
    
    init() {
        
    }
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        cinema_phone <- map["cinema_phone"]
        cinema_abb <- map ["cinema_abb"]
        cinema_id <- map["cinema_id"]
        cinema_name <- map["cinema_name"]
        cinema_image <- map["cinema_image"]
        cinema_rate <- map["cinema_rate"]
        cine_position <- map["cine_position"]
        cinema_address <- map["cinema_address"]
        cinema_link_book <- map["cinema_link_book"]
        
        
    }
}
