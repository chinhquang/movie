//
//  Person.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/2/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import Foundation
import ObjectMapper
class PersonDetail: Mappable{
    var id : Int?
   
    var name : String?
    var release_date : String?
    var birthday: String?
    var overview : String?
    var known_for_department : String?
    var profile_path: String?
    var biography : String?
    var place_of_birth : String?
    required init?(map: Map){
        
    }
    init() {
        
    }
    func mapping(map: Map) {
        release_date <- map["release_date"]
        overview <- map["overview"]
        id <- map["id"]
        place_of_birth <- map["place_of_birth"]
        name <- map["name"]
        biography <- map["biography"]
        birthday <- map["birthday"]
        known_for_department <- map["known_for_department"]
        profile_path <- map["profile_path"]
        
    }
}
