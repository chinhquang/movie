//
//  BiographySubview.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/2/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class BiographySubview: UIView {

    @IBOutlet var view: UIView!
    @IBOutlet weak var overviewHistory: UILabel!
    required init?(coder aDecoder: NSCoder) {
        super .init(coder: aDecoder)
        commonInit()
    }
    override init(frame: CGRect) {
        super .init(frame: frame)
        commonInit()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("BiographySubview", owner: self, options: nil)
        //UINib(nibName: "MovieDetailView", bundle: nil).instantiate(withOwner: self, options: nil)
        view.frame = self.bounds
        addSubview(view)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
