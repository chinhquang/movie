//
//  PeopleSubview.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/29/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit

class PeopleSubview: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var castCollectionview: UICollectionView!
    @IBOutlet weak var crewCollectionview: UICollectionView!
    @IBOutlet var view: UIView!
    required init?(coder aDecoder: NSCoder) {
        super .init(coder: aDecoder)
        commonInit()
    }
    override init(frame: CGRect) {
        super .init(frame: frame)
        commonInit()
    }
    private func commonInit(){
        Bundle.main.loadNibNamed("PeopleSubview", owner: self, options: nil)
        //UINib(nibName: "MovieDetailView", bundle: nil).instantiate(withOwner: self, options: nil)
        view.frame = self.bounds
        addSubview(view)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
