//
//  OverviewSubview.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/28/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit

class OverviewSubview: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var plot: UILabel!
    @IBOutlet weak var trailerCollectionView: UICollectionView!
    @IBOutlet weak var revenue: UILabel!
    @IBOutlet weak var runtime: UILabel!
    @IBOutlet weak var budget: UILabel!
    @IBOutlet weak var released_date: UILabel!
    @IBOutlet var view: UIView!
    required init?(coder aDecoder: NSCoder) {
        super .init(coder: aDecoder)
        commonInit()
    }
    override init(frame: CGRect) {
        super .init(frame: frame)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("OverviewSubview", owner: self, options: nil)
        //UINib(nibName: "MovieDetailView", bundle: nil).instantiate(withOwner: self, options: nil)
        view.frame = self.bounds
        addSubview(view)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func getHeightSize() -> CGFloat {
        let x =  self.plot.frame.height + self.stackView.frame.height + 254
        return x
    }
}
