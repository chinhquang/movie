//
//  PopularPeopleViewCell.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/2/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import SDWebImage
class PopularPeopleViewCell: UICollectionViewCell {

    @IBOutlet weak var tittle: UILabel!
    @IBOutlet weak var image: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setCell (detail : PersonDetail){
        if let image = detail.profile_path{
            let x = API.getFullURLOfImage(name: image)
            
            let Url = URL(string : x)
            self.image.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "avatar_placeholder"))
        }
        if let tittle = detail.name{
            self.tittle.text = tittle
        }
        
    }

}
