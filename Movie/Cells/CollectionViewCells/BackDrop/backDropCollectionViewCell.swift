//
//  backDropCollectionViewCell.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/2/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import SDWebImage
class backDropCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var tittle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setCell(image : String,tittle : String){
        let x = API.getFullURLOfImage(name: image)
        
        let Url = URL(string : x)
        self.image.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "movie_placeholder-103642"))
        
        
        
        
        self.tittle.text = tittle
    }
}
