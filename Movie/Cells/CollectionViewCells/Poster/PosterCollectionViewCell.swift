//
//  PosterCollectionViewCell.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/28/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
import SDWebImage
class PosterCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var subtittle: UILabel!
    @IBOutlet weak var tittle: UILabel!
    @IBOutlet weak var image: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setCell(image : String,tittle : String,subtittle:String){
        let x = API.getFullURLOfImage(name: image)
        let Url = URL(string : x)
        self.image.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "movie_placeholder-103642"))
        self.tittle.text = tittle
        self.subtittle.text = subtittle
        self.image.isUserInteractionEnabled = true
    }
    func setCell(detail : MovieDetail){
        if let image = detail.image{
            let x = API.getFullURLOfImage(name: image)
            let Url = URL(string : x)
            self.image.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "movie_placeholder-103642"))
        }else {
            self.image.image = #imageLiteral(resourceName: "movie_placeholder-103642")
        }
        if let tittle = detail.tittle{
            self.tittle.text = tittle
        }
        if let release_date = detail.release_date{
            self.subtittle.text = "\(release_date)"
        }
        self.image.isUserInteractionEnabled = true
        
    }
    func setCell(detail : CrewDetail){
        if let image = detail.profile_path{
            let x = API.getFullURLOfImage(name: image)
            let Url = URL(string : x)
            self.image.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "avatar_placeholder"))
        }else {
            self.image.image = #imageLiteral(resourceName: "avatar_placeholder")
        }
        if let tittle = detail.name{
            self.tittle.text = tittle
        }
        if let subtittle = detail.job{
            self.subtittle.text = "\(subtittle)"
        }
        self.image.isUserInteractionEnabled = true
    }
    func setCell(detail : CastDetail){
        if let image = detail.profile_path{
            let x = API.getFullURLOfImage(name: image)
            
            let Url = URL(string : x)
            self.image.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "avatar_placeholder"))
        }else {
            self.image.image = #imageLiteral(resourceName: "avatar_placeholder")
        }
        if let tittle = detail.name{
            self.tittle.text = tittle
        }
        if let subtittle = detail.character{
            self.subtittle.text = "\(subtittle)"
        }
        self.image.isUserInteractionEnabled = true
    }

    

}
