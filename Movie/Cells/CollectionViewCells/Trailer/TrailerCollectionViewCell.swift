//
//  TrailerCollectionViewCell.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/28/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView
class TrailerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var YTPlayerview: WKYTPlayerView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func getVideo(videoCode : String){
        YTPlayerview.load(withVideoId: videoCode)
        //webView.loadRequest(URLRequest(url: url!))
        
        
    }
}
