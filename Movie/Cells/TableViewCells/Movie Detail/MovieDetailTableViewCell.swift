//
//  MovieDetailTableViewCell.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/2/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import SDWebImage
class MovieDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var vote_average: UILabel!
    @IBOutlet weak var movieTittle: UILabel!
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var rating: RatingController!
    @IBOutlet weak var vote_count: UILabel!
    @IBOutlet weak var release_date: UILabel!
    
    @IBOutlet weak var watchTrailer: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    func setCell (detail : MovieDetail){
        if let tittle = detail.tittle{
            self.movieTittle.text = tittle
        }
        if let release_date = detail.release_date{
            self.release_date.text = "Date: \(release_date)"
        }
        if let vote_count = detail.vote_count{
            self.vote_count.text = "Vote: \(vote_count)"
        }
        
        if let vote_avg = detail.vote_average{
            self.vote_average.text = "(\(vote_avg))"
            let a = Int(vote_avg)
            self.rating.setStarsRating(rating: a)
        }
        
        if let image = detail.image{
            let x = API.getFullURLOfImage(name: image)
            
            let Url = URL(string : x)
            self.poster.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "movie_placeholder-103642"))
        }
        
        
    }
    
    
}
