//
//  TrailerTableViewCell.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/18/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView
class TrailerTableViewCell: UITableViewCell {

    @IBOutlet weak var youtubeTrailer: WKYTPlayerView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func getVideo(videoCode : String){
        
        youtubeTrailer.load(withVideoId: videoCode)
        //webView.loadRequest(URLRequest(url: url!))
        
        
    }
    
}
