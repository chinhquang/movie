//
//  SearchResultCell.swift
//  Movie
//
//  Created by Chính Trình Quang on 12/31/18.
//  Copyright © 2018 Chính Trình Quang. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {

    @IBOutlet weak var searchResult: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.backgroundView?.backgroundColor = UIColor.darkGray
    }
    func setCell(result : String){
        self.searchResult.text = result
    }
    
}
