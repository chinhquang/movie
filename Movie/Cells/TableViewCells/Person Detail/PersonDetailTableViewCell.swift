//
//  PersonDetailTableViewCell.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/7/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class PersonDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var overView: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCell(detail : PersonDetail, movieDetail : MovieDetail) {
        if let name = detail.name
        {
            self.name.text = name
        }
        if let date = movieDetail.release_date
        {
            self.date.text = "Date: \(date)"
        }
        if let overview = movieDetail.overview
        {
            self.overView.text = "OverView: \(overview)"
        }
        if let image = detail.profile_path{
            let x = API.getFullURLOfImage(name: image)
            let Url = URL(string : x)
            self.avatar.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "avatar_placeholder"))
        }else {
            self.avatar.image = #imageLiteral(resourceName: "avatar_placeholder")
        }
    }
    
}
