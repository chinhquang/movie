//
//  TheatreDetailTableViewCell.swift
//  Movie
//
//  Created by Chính Trình Quang on 1/10/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class TheatreDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var CinemaPhone: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCell (detail : CinemaDetail){
        if let name = detail.cinema_name{
            self.name.text = name
        }
        if let address = detail.cinema_address
        {
            self.address.text = "Address : \(address)"
        }
        if let phone = detail.cinema_phone
        {
            self.CinemaPhone.text = "Hotline: \(phone)"
        }
        if let image = detail.cinema_image{
            
            
            let Url = URL(string : "\(API.logoURL)\(image)")
            self.logo.sd_setImage(with: Url, placeholderImage: #imageLiteral(resourceName: "movie_placeholder-103642"))
        }else {
            self.logo.image = #imageLiteral(resourceName: "movie_placeholder-103642")
        }
    }
}
